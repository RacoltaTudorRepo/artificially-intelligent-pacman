def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    "*** YOUR CODE HERE ***"
    actionList = []
    visited = []
    pQueueNodes = util.PriorityQueue()
    state = problem.getStartState()
    visited.append(state)
    startNode=NodeStruct(state,(),0,())
    pQueueNodes.push(startNode,0.0)

    while True:
        node = pQueueNodes.pop()
        print node
        if problem.isGoalState(node.getState()) is True:
            while (node.getState()) != problem.getStartState():
                actionList.append(node.getAction())
                node=node.getParent()
            actionList.reverse()
            return actionList
        succesors = problem.getSuccessors(node.getState())
        for succ in succesors:
            if succ[0] not in visited:
                pQueueNodes.update(NodeStruct(succ[0], succ[1], succ[2]+0.0, node), node.getCost() + succ[2] + 0.0)
        visited.append(node.getState())



def breadthFirstSearch(problem):

    "*** YOUR CODE HERE ***"
    actionList = []
    visited = []
    stackNodes = util.Queue()
    state = problem.getStartState()
    visited.append(state)
    startNode=NodeStruct(state,(),0,())
    stackNodes.push(startNode)

    while True:
        node = stackNodes.pop()
        if problem.isGoalState(node.getState()) is True:
            while (node.getState()) != problem.getStartState():
                actionList.append(node.getAction())
                node=node.getParent()
            actionList.reverse()
            return actionList
        succesors = problem.getSuccessors(node.getState())
        for succ in succesors:
            if succ[0] not in visited:
                stackNodes.push(NodeStruct(succ[0],succ[1],succ[2],node))
        visited.append(node.getState())