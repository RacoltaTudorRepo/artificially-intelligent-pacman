# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util
import random




class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()

class NodeStruct:
    def __init__(self,state,action,cost,parent):
       self.state=state
       self.parent=parent
       self.action=action
       self.cost=cost
    def getState(self):
       return self.state
    def getCost(self):
       return self.cost
    def getAction(self):
       return self.action
    def getParent(self):
       return self.parent


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]



def depthFirstSearch(problem):

    "*** YOUR CODE HERE ***"
    actionList = []
    visited = []
    stackNodes = util.Stack()
    state = problem.getStartState()
    visited.append(state)
    startNode=NodeStruct(state,(),0,())
    stackNodes.push(startNode)

    while True:
        node = stackNodes.pop()
        if problem.isGoalState(node.getState()) is True:
            while (node.getState()) != problem.getStartState():
                actionList.append(node.getAction())
                node=node.getParent()
            actionList.reverse()
            print actionList
            return actionList
        succesors = problem.getSuccessors(node.getState())
        for succ in succesors:
            if succ[0] not in visited:
                stackNodes.push(NodeStruct(succ[0],succ[1],succ[2],node))

        visited.append(node.getState())

def randomSearch(problem):
    actionList=[]
    succesors=problem.getSuccessors(problem.getStartState())
    state,action,cost=random.choice(succesors)
    while(not(problem.isGoalState(state))):
       actionList.append(action)
       succesors=problem.getSuccessors(state)
       state,action,cost=random.choice(succesors)
    actionList.append(action)
    return actionList



def breadthFirstSearch(problem):

    "*** YOUR CODE HERE ***"
    actionList = []
    visited = []
    stackNodes = util.Queue()
    state = problem.getStartState()
    visited.append(state)
    startNode=NodeStruct(state,(),0,())
    stackNodes.push(startNode)

    while True:
        node = stackNodes.pop()
        if problem.isGoalState(node.getState()) is True:
            while (node.getState()) != problem.getStartState():
                actionList.append(node.getAction())
                node=node.getParent()
            actionList.reverse()
            return actionList
        succesors = problem.getSuccessors(node.getState())
        for succ in succesors:
            if succ[0] not in visited:
                if stackNodes.isMemberForStruct(succ[0]) is False:
                    stackNodes.push(NodeStruct(succ[0],succ[1],succ[2],node))
        visited.append(node.getState())


def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    "*** YOUR CODE HERE ***"
    actionList = []
    visited = []
    pQueueNodes = util.PriorityQueue()


    state = problem.getStartState()
    pQueueNodes.push((state,actionList,0.0),0.0)
    while True:
        (state, actionList,cost) = pQueueNodes.pop()
        #print state,'   ', cost
        if problem.isGoalState(state) == True:
            return actionList
        succesors = problem.getSuccessors(state)
        for succ in succesors:
            if succ[0] not in visited:
                pQueueNodes.update((succ[0], actionList +[succ[1]] ,cost+succ[2]+0.0),cost+succ[2]+0.0)
        visited.append(state)


def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** YOUR CODE HERE ***"
    actionList = []
    visited = []
    pQueueNodes = util.PriorityQueue()

    state = problem.getStartState()
    pQueueNodes.push((state, actionList, 0.0), heuristic(state,problem))
    while True:
        (state, actionList, cost) = pQueueNodes.pop()
         #print state,'   ', cost
        if problem.isGoalState(state) == True:
            return actionList
        succesors = problem.getSuccessors(state)
        for succ in succesors:
            if succ[0] not in visited:
                pQueueNodes.update((succ[0], actionList + [succ[1]], cost + succ[2] + 0.0), cost + succ[2] + 0.0 + heuristic(succ[0], problem))
        visited.append(state)


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
